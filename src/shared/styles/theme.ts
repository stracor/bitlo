export const colors = {
	primary: '#373C41',
	secondary: '#7D8083',
	background: '#22272B',
	textGray: '#828587',
	green: '#87CF84',
	red: '#FD605B',
	orange: '#F3AC7A',
};
