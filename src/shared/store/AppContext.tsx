import React, { createContext, useContext, useState, useEffect, useRef, useMemo, useCallback } from 'react';

import { getLocalStorage, setLocalStorage } from '@libs/localStoragePersist';
import { IFavoriteContext, IFavorites, IMarket, IWallet } from '@models/types';
import BottomSheet, { BottomSheetFlatList } from '@gorhom/bottom-sheet';
import { FlatList, Pressable, Text, View } from 'react-native';
import { BottomSheetMethods } from '@gorhom/bottom-sheet/lib/typescript/types';
import { Button, Dialog, RadioButton, TextInput } from 'react-native-paper';
import { colors } from '@styles/theme';
import ListItem from '@pages/MarketPage/components/ListItem';
import Icon from '@components/Icon';
import SquareCoinListItem from '@components/SquareCoinListItem';
import styles from './style';

const AppContext = createContext({});

const useAppContext: () => IFavoriteContext = () => useContext(AppContext);

const AppContextProvider = ({ children }: any) => {
	const [markets, setMarkets] = useState<IMarket[]>([]);

	const [favorites, setFavorites] = useState<IFavorites>(getLocalStorage('favorites', {}));
	const [wallet, setWallet] = useState<IWallet>(getLocalStorage('wallet', {}));

	const resetStates = () => {
		setFavorites({});
		setWallet({});
	};

	useEffect(() => {
		setLocalStorage('favorites', favorites);
	}, [favorites]);

	useEffect(() => {
		setLocalStorage('wallet', wallet);
	}, [wallet]);

	const buyCoin = (item: IMarket, balance: number) => {
		setWallet(x => {
			x[item?.marketCode] = { ...item, balance: (x?.[item?.marketCode]?.balance ?? 0) + balance };
			return { ...x };
		});
	};

	//
	const [price, setPrice] = useState('');
	const [selectedMarket, setSelectedMarket] = useState<IMarket>();
	const [marketPickerVisible, setMarketPickerVisible] = useState(false);
	const showMarketPickerModal = () => setMarketPickerVisible(true);
	const hideMarketPickerModal = () => setMarketPickerVisible(false);

	// ref
	const bottomSheetRef = useRef<BottomSheetMethods>(null);

	// variables
	const snapPoints = useMemo(() => [400, 600], []);

	const openExchangeModal = () => {
		bottomSheetRef?.current?.snapToIndex(0);
	};

	const addOrRemoveFavorites = (market: IMarket | any) => {
		setFavorites((f: IFavorites) => {
			if (f?.[market.marketCode]) {
				delete f?.[market.marketCode];
			} else {
				f[market.marketCode] = market;
			}

			return { ...f };
		});
	};

	const renderItem = ({ item, index }) => (
		<ListItem
			item={item}
			onPress={() => {
				setSelectedMarket(item);
				hideMarketPickerModal();
			}}
		/>
	);

	const renderWalletItem = props => <SquareCoinListItem {...props} />;
	const bottomSheetFlatListKeyExtractor = item => item?.marketCode + 'WalletBottom';
	const coinListKeyExtractor = item => item?.marketCode + 'conListBottom';

	return (
		<AppContext.Provider value={{ favorites, addOrRemoveFavorites, openExchangeModal, buyCoin, markets, setMarkets, wallet, setWallet, resetStates }}>
			{children}
			<BottomSheet ref={bottomSheetRef} index={-1} enablePanDownToClose snapPoints={snapPoints}>
				<View style={styles.bottomSheetContainer}>
					<View style={styles.bottomSheetTop}>
						<TextInput label={'Miktar'} value={price + ''} onChangeText={setPrice} keyboardType="numeric" mode="outlined" />
						<View style={styles.buttonContainer}>
							<Button color={colors.orange} style={styles.selectCoinButton} onPress={showMarketPickerModal} mode="outlined">
								{selectedMarket?.marketCode?.getName() ?? 'Bir Coin Seçin'}
							</Button>
							{selectedMarket && (
								<Button onPress={() => setSelectedMarket(null)}>
									<Icon name="trash : evil" size={25} color={colors.red} />
								</Button>
							)}
						</View>
						{selectedMarket && (
							<>
								<View style={styles.listItemContainer}>
									<ListItem item={selectedMarket} onPress={() => {}} />
								</View>

								<Text numberOfLines={1} adjustsFontSizeToFit style={styles.priceTotal}>
									{(parseInt(selectedMarket?.currentQuote) || 0) * (parseInt(price) || 0)} ₺
								</Text>
								<Button mode="outlined" color={'white'} style={styles.purchaseButton} onPress={() => buyCoin(selectedMarket, parseInt(price))}>
									Satın Al
								</Button>
							</>
						)}
						<Text style={styles.descriptionText}>Cüzdanını görmek için yukarı kaydır.</Text>
					</View>

					<View style={styles.bottomSheetBottom}>
						<BottomSheetFlatList
							data={Object.values(wallet)}
							horizontal
							renderItem={renderWalletItem}
							keyExtractor={bottomSheetFlatListKeyExtractor}
							style={{ flex: 1 }}
						/>
					</View>
				</View>
			</BottomSheet>
			<Dialog style={{ backgroundColor: colors.background }} visible={marketPickerVisible} onDismiss={hideMarketPickerModal}>
				<Dialog.Title style={{ color: 'white' }}>Coin Seç</Dialog.Title>
				<Dialog.ScrollArea>
					<FlatList renderItem={renderItem} keyExtractor={coinListKeyExtractor} data={markets} />
				</Dialog.ScrollArea>
			</Dialog>
		</AppContext.Provider>
	);
};

export { AppContextProvider, useAppContext };
