import { colors } from '@styles/theme';
import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
	bottomSheetContainer: {
		height: 600,
		width: '100%',
	},
	bottomSheetTop: {
		height: 400,
		width: '100%',
		padding: 20,
	},
	buttonContainer: {
		marginTop: 10,
		flexDirection: 'row',
	},
	selectCoinButton: {
		backgroundColor: colors.primary,
		flex: 1,
	},
	listItemContainer: {
		backgroundColor: colors.primary,
		paddingBottom: 10,
		marginTop: 10,
		borderRadius: 5,
	},
	priceTotal: {
		fontSize: 45,
		alignSelf: 'center',
		fontWeight: 'bold',
	},
	purchaseButton: {
		backgroundColor: colors.green,
	},
	descriptionText: {
		alignSelf: 'center',
	},
	bottomSheetBottom: {
		height: 200,
		paddingTop: 20,
	},
});

export default styles;
