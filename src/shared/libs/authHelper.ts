import firestore from '@react-native-firebase/firestore';

export const usersCollection = firestore().collection('Users');

export const getUserData = id => usersCollection.doc(id).get();

export const setUserData = (id, data) =>
	new Promise((resolve, reject) => {
		usersCollection.doc(id).set(data, { merge: true }).then(resolve).catch(reject);
	});
