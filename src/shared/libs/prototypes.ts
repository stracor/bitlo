declare global {
	interface String {
		getName(): string;
	}
}

String.prototype.getName = function () {
	let d = String(this);

	return d?.split('-')?.[0];
};

export {};
