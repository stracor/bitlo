import { MMKV } from 'react-native-mmkv';

export const storage = new MMKV();

export const getLocalStorage = (key: string, initialValue: any) => {
	const value = storage.getString(key) ?? initialValue;
	try {
		return JSON.parse(value);
	} catch (error) {
		return value;
	}
};

export const setLocalStorage = (key: string, value: any) => {
	typeof value == 'object' ? storage.set(key, JSON.stringify(value)) : storage.set(key, value);
};
