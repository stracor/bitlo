import React from 'react';

//Third party libraries
import FastImage from 'react-native-fast-image';
import { SvgCssUri } from 'react-native-svg';

const CoinIcon = ({ name = '', width = 30, height = 30 }) => {
	return <SvgCssUri width={width} height={height} uri={`https://static.bitlo.com/cryptologossvg/${name.getName()?.toLocaleLowerCase()}.svg`} />;
};

export default CoinIcon;
