import React from 'react';
import { Text, View } from 'react-native';

//Components
import CoinIcon from '@components/CoinIcon';

import { IWalletItem } from '@models/types';

//Styles
import styles from './style';
import { colors } from '@styles/theme';

const SquareCoinListItem = ({ item }: { item: IWalletItem }) => {
	return (
		<View style={[styles.container, { backgroundColor: item?.color ?? colors.background }]}>
			<View style={styles.iconContainer}>
				<CoinIcon name={item.marketCode} width={50} height={50} />
			</View>
			<Text style={styles.text} numberOfLines={1} adjustsFontSizeToFit>
				{item?.balance} {item?.marketCode?.getName()}
			</Text>
		</View>
	);
};

export default SquareCoinListItem;
