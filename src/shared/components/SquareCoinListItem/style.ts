import { StyleSheet } from 'react-native';

import { colors } from '@styles/theme';

const styles = StyleSheet.create({
	container: {
		margin: 10,
		backgroundColor: colors.background,
		height: 90,
		width: 90,
		borderRadius: 10,
		alignItems: 'center',
		padding: 5,
	},
	iconContainer: {
		backgroundColor: colors.primary,
		padding: 5,
		borderRadius: 5,
	},
	text: {
		color: 'white',
		fontSize: 15,
		fontWeight: 'bold',
	},
});

export default styles;
