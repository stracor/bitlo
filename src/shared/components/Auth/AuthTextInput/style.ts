import { colors } from '@styles/theme';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	container: {
		width: '100%',
		height: 45,
		backgroundColor: '#494C4F',
		flexDirection: 'row',
		borderRadius: 5,
		overflow: 'hidden',
		marginVertical: 5,
	},
	iconContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.primary,
		width: 45,
	},
	input: {
		flex: 1,
		color: 'white',
		paddingLeft: 10,
	},
	showPassButton: {
		alignSelf: 'center',
		marginRight: 10,
		padding: 10,
	},
});

export default styles;
