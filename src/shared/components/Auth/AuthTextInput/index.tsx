import React, { useState } from 'react';
import { TextInput, View, Pressable } from 'react-native';

//Components
import Icon from '@components/Icon';

//Styles
import { colors } from '@styles/theme';
import styles from './style';

const AuthTextInput = ({ isPassword = false, value, onChangeText, iconName = null, placeholder = '' }) => {
	const [show, setShow] = useState(isPassword);

	const togglePassword = () => setShow(!show);

	return (
		<View style={styles.container}>
			{iconName && (
				<View style={styles.iconContainer}>
					<Icon name={iconName} size={16} color={colors.textGray} />
				</View>
			)}
			<TextInput
				style={styles.input}
				placeholder={placeholder}
				secureTextEntry={show}
				placeholderTextColor={colors.textGray}
				value={value}
				onChangeText={onChangeText}
			/>
			{isPassword && (
				<Pressable onPress={togglePassword} style={styles.showPassButton}>
					<Icon name={show ? 'eye-off : ion' : 'eye : ion'} size={20} color="white" />
				</Pressable>
			)}
		</View>
	);
};

export default AuthTextInput;
