import { colors } from '@styles/theme';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	container: ({ color }) => ({
		width: '100%',
		height: 45,
		backgroundColor: color,
		flexDirection: 'row',
		borderRadius: 5,
		overflow: 'hidden',
		marginVertical: 5,
		alignItems: 'center',
		justifyContent: 'center',
	}),
	buttonText: {
		color: 'white',
	},
	indicator: {
		marginRight: 10,
	},
});

export default styles;
