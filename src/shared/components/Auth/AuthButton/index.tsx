import React, { useState } from 'react';
import { TextInput, View, Pressable, Text } from 'react-native';

//Components
import Icon from '@components/Icon';

//Styles
import styles from './style';
import { ActivityIndicator } from 'react-native-paper';

const AuthButton = ({ text, onPress, color = '#494C4F', loading = false }) => {
	return (
		<Pressable onPress={onPress} disabled={loading} style={styles.container({ color })}>
			{loading && <ActivityIndicator style={styles.indicator} color="white" size={17} />}
			<Text style={styles.buttonText}>{text}</Text>
		</Pressable>
	);
};

export default AuthButton;
