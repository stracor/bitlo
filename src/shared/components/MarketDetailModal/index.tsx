import React from 'react';
import { Pressable, View } from 'react-native';

//Third party libraries
import { Dialog, Portal, Text } from 'react-native-paper';

//Store
import { useAppContext } from '@store/AppContext';

//Components
import CoinIcon from '@components/CoinIcon';
import Icon from '@components/Icon';

//Styles
import styles from './style';
import { IMarketDetailModal } from '@models/types';
import { colors } from '@styles/theme';

const MarketDetailModal = ({ item, unSelectItem, navigate }: IMarketDetailModal) => {
	const { favorites, addOrRemoveFavorites } = useAppContext();

	const favoriteHandler = () => addOrRemoveFavorites(item);

	return (
		<Portal>
			<Dialog visible={!!item} onDismiss={unSelectItem}>
				<Dialog.Content>
					<View style={styles.dialogHeader}>
						<CoinIcon name={item?.marketCode} />
						<Text style={styles.dialogTitle}>{item?.marketCode}</Text>
						<Pressable onPress={favoriteHandler}>
							<Icon
								name={favorites?.[item?.marketCode] ? 'star : font' : 'star-o : font'}
								color={favorites?.[item?.marketCode] ? colors.orange : colors.background}
								size={30}
							/>
						</Pressable>
					</View>

					<View style={styles.row}>
						<View style={styles.ais}>
							<Text style={styles.title}>Güncel Fiyat</Text>
							<Text style={styles.value}>{item?.currentQuote}</Text>
						</View>
						<View style={styles.aie}>
							<Text style={styles.title}>24 Saatlik Değişim</Text>
							<Text style={styles.value}>%{item?.change24hPercent}</Text>
						</View>
					</View>

					<View style={styles.row}>
						<View style={styles.ais}>
							<Text style={styles.title}>24 Saat En Yüksek</Text>
							<Text style={styles.value}>{item?.highestQuote24h}</Text>
						</View>
						<View style={styles.aie}>
							<Text style={styles.title}>24 Saat En Düşük</Text>
							<Text style={styles.value}>{item?.lowestQuote24h}</Text>
						</View>
					</View>

					<View style={styles.row}>
						<View style={styles.ais}>
							<Text style={styles.title}>24 Saat Ortalama</Text>
							<Text style={styles.value}>{item?.weightedAverage24h}</Text>
						</View>
						<View style={styles.aie}>
							<Text style={styles.title}>24 Saat Hacim</Text>
							<Text style={styles.value}>{item?.volume24h}</Text>
						</View>
					</View>

					<View style={styles.row}>
						<Pressable style={styles.navigateButton} onPress={navigate}>
							<Text style={styles.buttonText}>Detay</Text>
							<Icon name="chevron-right : materialcomm" size={25} />
						</Pressable>
					</View>
				</Dialog.Content>
			</Dialog>
		</Portal>
	);
};

export default MarketDetailModal;
