import { StyleSheet } from 'react-native';

import { colors } from '@styles/theme';

const styles = StyleSheet.create({
	dialogHeader: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	dialogTitle: {
		fontWeight: 'bold',
		fontSize: 15,
	},
	row: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginVertical: 10,
		alignItems: 'center',
	},
	title: {
		color: colors.textGray,
	},
	value: {
		fontWeight: 'bold',
	},
	ais: {
		alignItems: 'flex-start',
	},
	aie: {
		alignItems: 'flex-end',
	},
	navigateButton: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	buttonText: {
		fontWeight: 'bold',
		fontSize: 15,
		marginRight: 0,
	},
});

export default styles;
