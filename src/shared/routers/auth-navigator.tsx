import React from 'react';

//Third party libraries
import { createStackNavigator } from '@react-navigation/stack';

//Pages
import LoginPage from '@pages/Auth/LoginPage';
import RegisterPage from '@pages/Auth/RegisterPage';
import AuthWelcomePage from '@pages/Auth/AuthWelcomePage';

const AuthNavigator = () => {
	const Stack = createStackNavigator();

	return (
		<Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName={'AuthWelcomePage'}>
			<Stack.Screen name="AuthWelcomePage" component={AuthWelcomePage} />
			<Stack.Screen name="LoginPage" component={LoginPage} />
			<Stack.Screen name="RegisterPage" component={RegisterPage} />
		</Stack.Navigator>
	);
};

export default AuthNavigator;
