import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';

//Pages
import LoadingPage from '@pages/LoadingPage';
import CoinDetail from '@pages/CoinDetail';

//Navigator
import AppNavigator from './app-navigator';
import AuthNavigator from './auth-navigator';

const RootNavigator = ({ loggedIn }) => {
	const Stack = createStackNavigator();

	return (
		<Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName={loggedIn ? 'AppNavigator' : 'AuthNavigator'}>
			<Stack.Screen name="Loading" component={LoadingPage} />
			<Stack.Screen name="AuthNavigator" component={AuthNavigator} />

			<Stack.Group>
				<Stack.Screen name="AppNavigator" component={AppNavigator} />
				<Stack.Screen name="CoinDetail" component={CoinDetail} />
			</Stack.Group>
		</Stack.Navigator>
	);
};

export default RootNavigator;
