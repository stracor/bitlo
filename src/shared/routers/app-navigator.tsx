import React, { useContext } from 'react';
import { Pressable, Text, View } from 'react-native';

//Third party libraries
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

//Pages
import MarketPage from '@pages/MarketPage';
import WalletPage from '@pages/WalletPage';
import FavoritePage from '@pages/FavoritePage';
import ProfilePage from '@pages/ProfilePage';

//Components
import Icon from '@components/Icon';

//Styles
import { colors } from '@styles/theme';
import { useAppContext } from '@store/AppContext';

const AppNavigator = () => {
	const Tab = createBottomTabNavigator();

	const NullComp = () => null;

	const { openExchangeModal } = useAppContext();

	const tabBarIcon =
		(name, color = null) =>
		props =>
			<Icon name={name} {...props} color={color || props?.color} />;

	return (
		<Tab.Navigator
			screenOptions={{
				headerShown: false,
				tabBarShowLabel: false,
				tabBarActiveTintColor: 'white',
				tabBarInactiveTintColor: colors.secondary,
				tabBarStyle: {
					position: 'absolute',
					bottom: 10,
					left: 10,
					right: 10,
					borderRadius: 10,
					backgroundColor: colors.primary,
					borderTopWidth: 0,
					elevation: 0,
				},
			}}
			initialRouteName="MarketPage">
			<Tab.Screen
				name="MarketPage"
				options={{
					tabBarIcon: tabBarIcon('coins : font5'),
				}}
				component={MarketPage}
			/>

			<Tab.Screen
				name="FavoritePage"
				options={{
					tabBarIcon: tabBarIcon('star : font'),
				}}
				component={FavoritePage}
			/>

			<Tab.Screen
				name="AddButton"
				component={NullComp}
				options={{
					tabBarIcon: tabBarIcon('exchange : font', 'white'),
					tabBarButton: props => (
						<Pressable
							style={{
								margin: 10,
								marginTop: -10,
								backgroundColor: colors.secondary,
								height: 50,
								width: 50,
								borderRadius: 10,
								alignItems: 'center',
								justifyContent: 'center',
							}}
							hitSlop={10}
							onPress={openExchangeModal}>
							{props.children}
						</Pressable>
					),
				}}
			/>

			<Tab.Screen
				name="WalletPage"
				options={{
					tabBarIcon: tabBarIcon('wallet : entypo'),
				}}
				component={WalletPage}
			/>

			<Tab.Screen
				name="ProfilePage"
				options={{
					tabBarIcon: tabBarIcon('user : font'),
				}}
				component={ProfilePage}
			/>
		</Tab.Navigator>
	);
};

export default AppNavigator;
