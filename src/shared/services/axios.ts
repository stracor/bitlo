//Third Party
import axios from 'axios';

const AxiosInstance = axios.create();

AxiosInstance.defaults.baseURL = 'https://api4.bitlo.com';

AxiosInstance.interceptors.request.use(
	async config => {
		return {
			...config,
			headers: {
				...config.headers,
				'content-type': 'application/json',
			},
		};
	},
	error => {
		return Promise.reject(error);
	},
);
AxiosInstance.interceptors.response.use(
	response => {
		return response;
	},
	async err => {
		if (err?.response?.status === 401) {
			// #TODO:Logout
		}
		return Promise.reject(err);
	},
);
export default AxiosInstance;
