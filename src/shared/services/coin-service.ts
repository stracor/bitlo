import axios from './axios';

class CoinService {
	async getMarkets() {
		return await axios.get(`market/ticker/all`); // #TODO: Language
	}

	async getMarketDetail(params: { market: string; depth: number }) {
		return await axios.get(`market/orderbook`, { params }); // #TODO: Language
	}
}

export default new CoinService();
