import { IMarket } from '@models/types';
export type IMarket = {
	/** Coinin market kodu */
	marketCode: string;

	/** Coinin değeri */
	currentQuote: string;

	/** Coinin son 24 saat içerisindeki değişimi */
	change24h: string;

	/** Coinin son 24 saat içerisindeki yüzdelik değişimi */
	change24hPercent: string;

	/** Coinin son 24 saat içerisinde gördüğü en yüksek değer */
	highestQuote24h: string;

	/** Coinin son 24 saat içerisinde gördüğü en düşük değer */
	lowestQuote24h: string;

	/** Coinin son 24 saat içerisindeki değerinin ortalaması */
	weightedAverage24h: string;

	/** Coinin son 24 saat içerisindeki hacmi */
	volume24h: string;
};

export type IMarketDetail = {
	sequenceId?: number;
	bids?: Array<INode>;
	asks?: Array<INode>;
};

export type INode = {
	[k in '0' | '1']: number;
};

export type IMarketDetailModal = {
	item: IMarket;
	unSelectItem: () => void;
	navigate: () => void;
};

export type IFavorites = {
	[k: string]: IMarket;
};

export type IFavoriteContext = {
	markets?: Array<IMarket>;
	setMarkets?: React.Dispatch<React.SetStateAction<IMarket[]>>;
	favorites?: IFavorites;
	addOrRemoveFavorites?: (k: IMarket) => void;
	openExchangeModal?: () => void;
	buyCoin?: () => (item: IMarket, balance: number) => void;
	wallet?: IWallet;
	resetStates?: () => void;
};

export type IWallet = {
	[k: string]: IWalletItem;
};

export type IWalletItem = {
	balance: number;
} & IMarket;
