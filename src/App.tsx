import React, { useEffect, useRef, useState } from 'react';
import { LogBox, View } from 'react-native';

//Third party libraries
import { NavigationContainer } from '@react-navigation/native';
import { Provider as PaperProvider } from 'react-native-paper';
import auth from '@react-native-firebase/auth';
import { GestureHandlerRootView } from 'react-native-gesture-handler';

//Navigator
import RootNavigator from '@routers/root-navigator';
import { AppContextProvider } from '@store/AppContext';

const App = () => {
	LogBox.ignoreAllLogs();

	const [initializing, setInitializing] = useState(true);
	const [user, setUser] = useState();

	// Handle user state changes
	function onAuthStateChanged(usr) {
		setUser(usr);
		if (initializing) setInitializing(false);
	}

	useEffect(() => {
		const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
		return subscriber; // unsubscribe on unmount
	}, []);

	if (initializing) return null;

	return (
		<GestureHandlerRootView style={{ flex: 1 }}>
			<AppContextProvider>
				<PaperProvider>
					<NavigationContainer>
						<RootNavigator loggedIn={user} />
					</NavigationContainer>
				</PaperProvider>
			</AppContextProvider>
		</GestureHandlerRootView>
	);
};

export default App;
