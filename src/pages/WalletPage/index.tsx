import React, { useCallback } from 'react';
import { Dimensions, FlatList, Text, View } from 'react-native';

//Third party libraries
import { DonutChart } from 'react-native-circular-chart';

//Components
import SquareCoinListItem from '@components/SquareCoinListItem';

//Store
import { useAppContext } from '@store/AppContext';

//Styles
import styles from './styles';
import { stringToColour } from '@libs/generalHelper';

const { width } = Dimensions.get('screen');

const WalletPage = () => {
	const { wallet } = useAppContext();

	const data = Object.values(wallet).map(i => ({
		...i,
		name: i?.marketCode?.getName(),
		value: i?.balance,
		color: stringToColour(i?.marketCode + i?.currentQuote),
	}));

	const renderItem = props => <SquareCoinListItem {...props} />;

	return (
		<View style={styles.container}>
			{data?.length > 0 ? (
				<DonutChart
					data={data}
					strokeWidth={15}
					radius={120}
					containerWidth={width}
					containerHeight={300}
					type="butt"
					startAngle={1}
					endAngle={360}
					animationType="fade"
				/>
			) : (
				<Text style={styles.description}>Cüzdanınız boş. Devam edebilmek için coin satın alın.</Text>
			)}
			<FlatList contentContainerStyle={styles.contentContainerStyle} numColumns={Math.floor(width / 110)} data={data} renderItem={renderItem} />
		</View>
	);
};

export default WalletPage;
