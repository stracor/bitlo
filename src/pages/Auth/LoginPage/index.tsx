import React, { useState } from 'react';
import { Alert, Pressable, Text, View } from 'react-native';

//Third party libraries
import auth from '@react-native-firebase/auth';

//Components
import AuthButton from '@components/Auth/AuthButton';
import AuthTextInput from '@components/Auth/AuthTextInput';

//Styles
import { colors } from '@styles/theme';
import styles from './style';
import { getUserData } from '@libs/authHelper';

const LoginPage = ({ navigation }) => {
	const [email, setEmail] = useState('oguzhankelees@gmail.com');
	const [password, setPassword] = useState('123123');

	const [loading, setLoading] = useState(false);

	const login = () => {
		setLoading(true);
		auth()
			.signInWithEmailAndPassword(email, password)
			.then(async res => {
				navigation.replace('AppNavigator');
			})
			.catch(err => {
				Alert.alert('Hata', 'Giriş Başarısız');
			})
			.finally(() => {
				setLoading(false);
			});
	};

	const navigate = routeName => () => navigation.navigate(routeName);
	return (
		<View style={styles.container}>
			<Text style={styles.title}>Giriş Yap</Text>
			<AuthTextInput value={email} onChangeText={setEmail} iconName="email : fontisto" placeholder="Email" />
			<AuthTextInput value={password} onChangeText={setPassword} iconName="lock : entypo" placeholder="Parola" isPassword />
			<AuthButton loading={loading} text={'Giriş Yap'} color={colors.green} onPress={login} />
			<Pressable onPress={navigate('RegisterPage')} style={styles.registerTextContainer}>
				<Text style={styles.registerText}>Hemen Bir Hesap Oluştur</Text>
			</Pressable>
		</View>
	);
};

export default LoginPage;
