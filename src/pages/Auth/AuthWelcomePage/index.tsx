import { colors } from '@styles/theme';
import React from 'react';
import { Pressable, Text, View } from 'react-native';

//Third party libraries
import FastImage from 'react-native-fast-image';

//Styles
import styles from './style';

const AuthWelcomePage = ({ navigation }) => {
	const navigate = name => () => navigation.navigate(name);

	return (
		<View style={styles.container}>
			<View style={styles.top}>
				<FastImage source={require('@assets/images/logo.png')} style={styles.logo} />
				<Text style={styles.slogan}>Kripto Para Borsası</Text>
			</View>
			<View style={styles.bottom}>
				<Pressable style={styles.button(colors.green)} onPress={navigate('LoginPage')}>
					<Text style={styles.buttonText}>Giriş Yap</Text>
				</Pressable>
				<Pressable style={styles.button(colors.red)} onPress={navigate('RegisterPage')}>
					<Text style={styles.buttonText}>Hesap Oluştur</Text>
				</Pressable>
			</View>
		</View>
	);
};

export default AuthWelcomePage;
