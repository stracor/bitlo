import { StyleSheet } from 'react-native';
import { colors } from '@styles/theme';

const MAX_WIDTH = 400;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: colors.background,
		justifyContent: 'center',
		alignItems: 'center',
	},
	top: {
		maxWidth: MAX_WIDTH,
		alignItems: 'center',
		justifyContent: 'center',
		marginBottom: 50,
	},
	slogan: {
		color: colors.textGray,
		fontSize: 15,
		marginTop: 5,
	},
	logo: {
		width: 150,
		height: 50,
	},
	bottom: {
		maxWidth: MAX_WIDTH,
		width: '100%',
	},
	buttonText: {
		color: 'white',
		fontSize: 15,
	},
	button: color => ({
		margin: 20,
		padding: 10,
		backgroundColor: color + '40',
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 20,
		borderWidth: 2,
		borderColor: color,
	}),
});

export default styles;
