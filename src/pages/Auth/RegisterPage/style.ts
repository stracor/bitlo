import { StyleSheet } from 'react-native';
import { colors } from '@styles/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: colors.background,
		padding: 10,
		justifyContent: 'center',
	},
	title: {
		marginTop: 20,
		marginBottom: 30,
		marginLeft: 20,
		color: 'white',
		fontWeight: 'bold',
		fontSize: 15,
	},
	loginTextContainer: {
		marginTop: 20,
		alignSelf: 'flex-end',
	},
	loginText: {
		color: 'white',
	},
});

export default styles;
