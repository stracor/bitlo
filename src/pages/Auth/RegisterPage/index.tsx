import React, { useState } from 'react';
import { Alert, Pressable, Text, View } from 'react-native';

//Third party libraries
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

//Components
import AuthButton from '@components/Auth/AuthButton';
import AuthTextInput from '@components/Auth/AuthTextInput';

//Styles
import { colors } from '@styles/theme';
import styles from './style';
import { setUserData, usersCollection } from '@libs/authHelper';

const RegisterPage = ({ navigation }) => {
	const [name, setName] = useState('');
	const [surName, setSurName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [rePassword, setRePassword] = useState('');

	const [loading, setLoading] = useState(false);

	const register = () => {
		if ([name, surName, email, password, rePassword].includes('')) {
			Alert.alert('Hata', 'Lütfen boş alan bırakmayın.');
			return;
		}

		setLoading(true);
		auth()
			.createUserWithEmailAndPassword(email, password)
			.then(async res => {
				navigate('AppNavigation');
				setUserData(res?.user?.uid, { name, surName });
			})
			.catch(err => {
				Alert.alert('Hata', 'Hesap oluşturulamadı.');
			})
			.finally(() => {
				setLoading(false);
			});
	};

	const navigate = routeName => () => navigation.navigate(routeName);

	return (
		<View style={styles.container}>
			<Text style={styles.title}>Hesap Oluştur</Text>
			<AuthTextInput value={name} onChangeText={setName} iconName="user : font5" placeholder="Ad" />
			<AuthTextInput value={surName} onChangeText={setSurName} iconName="user : font5" placeholder="Soyad" />
			<AuthTextInput value={email} onChangeText={setEmail} iconName="email : fontisto" placeholder="Email" />
			<AuthTextInput value={password} onChangeText={setPassword} iconName="lock : entypo" placeholder="Parola" isPassword />
			<AuthTextInput value={rePassword} onChangeText={setRePassword} iconName="lock : entypo" placeholder="Parola Tekrar" isPassword />
			<AuthButton loading={loading} text={'Kayıt ol'} color={colors.green} onPress={register} />
			<Pressable onPress={navigate('LoginPage')} style={styles.loginTextContainer}>
				<Text style={styles.loginText}>Bir hesabın mı var? Giriş Yap</Text>
			</Pressable>
		</View>
	);
};

export default RegisterPage;
