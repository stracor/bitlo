import React, { useEffect, useState } from 'react';
import { Alert, FlatList, Text, View } from 'react-native';

//Types
import { IMarket, IMarketDetail } from '@models/types';

//Styles and sertvices
import styles from './styles';
import coinService from '@services/coin-service';
import Row from './components/Row';
import CoinIcon from '@components/CoinIcon';

const CoinDetail = ({ route }) => {
	const { change24h, change24hPercent, currentQuote, highestQuote24h, lowestQuote24h, marketCode, volume24h, weightedAverage24h }: IMarket =
		route?.params?.item;

	const [data, setData] = useState<IMarketDetail>({});

	const currencyFormatter = new Intl.NumberFormat('tr-TR', { style: 'currency', currency: marketCode?.split('-')?.[1] || 'TRY' });
	const unitFormatter = new Intl.NumberFormat('tr-TR', { maximumFractionDigits: 10, minimumFractionDigits: 10 });

	const listData = data?.asks?.map((item, index) => {
		return {
			ask: {
				0: currencyFormatter.format(data?.bids?.[index]?.[0]),
				1: unitFormatter.format(data?.bids?.[index]?.[1]),
				2: currencyFormatter.format(data?.bids?.[index]?.[0] * data?.bids?.[index]?.[1]),
			},
			bid: {
				0: currencyFormatter.format(item?.[0]),
				1: unitFormatter.format(item?.[1]),
				2: currencyFormatter.format(item?.[0] * item?.[1]),
			},
		};
	});

	useEffect(() => {
		getMarketDetail();
	}, [marketCode]);

	const getMarketDetail = () => {
		coinService
			.getMarketDetail({ market: marketCode, depth: 50 })
			.then(res => {
				setData(res?.data);
			})
			.catch(err => {
				Alert.alert('Hata', 'Market verileri gelmedi');
			});
	};

	const renderItem = props => <Row {...props} />;

	const keyExtractor = item => item?.ask?.[0] + 'Detail' + item?.bid?.[0];

	return (
		<View style={styles.container}>
			<View style={styles.detailContainer}>
				<CoinIcon name={marketCode} height={55} width={55} />
				<Text style={styles.title}>{marketCode?.getName()}</Text>
			</View>
			<FlatList data={listData} renderItem={renderItem} keyExtractor={keyExtractor} />
		</View>
	);
};

export default CoinDetail;
