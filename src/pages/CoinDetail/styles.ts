import { StyleSheet } from 'react-native';

import { colors } from '@styles/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: colors.background,
	},
	detailContainer: {
		padding: 10,
		alignSelf: 'center',
		alignItems: 'center',
	},
	title: {
		fontSize: 20,
		color: 'white',
		fontWeight: 'bold',
		marginTop: 10,
	},
});

export default styles;
