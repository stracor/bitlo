import { StyleSheet } from 'react-native';

import { colors } from '@styles/theme';
const styles = StyleSheet.create({
	container: {
		paddingVertical: 5,
		marginHorizontal: 10,
		marginVertical: 5,
		backgroundColor: colors.primary,
	},
	row: {
		flexDirection: 'row',
		flex: 1,
	},
	column: {
		alignItems: 'center',
		flex: 1,
	},
	title: {
		color: 'white',
		fontWeight: 'bold',
		fontSize: 10,
	},
	text: {
		color: colors.textGray,
		fontSize: 12,
	},
});

export default styles;
