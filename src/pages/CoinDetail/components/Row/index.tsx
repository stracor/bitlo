import { colors } from '@styles/theme';
import React from 'react';
import { Text, View } from 'react-native';

//Styles
import styles from './style';

const Row = ({ item }) => {
	return (
		<View style={styles.container}>
			<View style={styles.row}>
				<View style={styles.column}>
					<Text numberOfLines={1} adjustsFontSizeToFit style={styles.title}>
						Fiyat
					</Text>
				</View>
				<View style={styles.column}>
					<Text numberOfLines={1} adjustsFontSizeToFit style={styles.title}>
						Miktar
					</Text>
				</View>
				<View style={styles.column}>
					<Text numberOfLines={1} adjustsFontSizeToFit style={styles.title}>
						Toplam
					</Text>
				</View>
			</View>

			<View style={styles.row}>
				<View style={styles.column}>
					<Text numberOfLines={1} adjustsFontSizeToFit style={[styles.text, { color: colors.green }]}>
						{item?.ask?.[0] || '-'}
					</Text>
				</View>
				<View style={styles.column}>
					<Text numberOfLines={1} adjustsFontSizeToFit style={styles.text}>
						{item?.ask?.[1] || '-'}
					</Text>
				</View>
				<View style={styles.column}>
					<Text numberOfLines={1} adjustsFontSizeToFit style={styles.text}>
						{item?.ask?.[2] || '-'}
					</Text>
				</View>
			</View>
			<View style={styles.row}>
				<View style={styles.column}>
					<Text numberOfLines={1} adjustsFontSizeToFit style={[styles.text, { color: colors.red }]}>
						{item?.bid?.[0] || '-'}
					</Text>
				</View>
				<View style={styles.column}>
					<Text numberOfLines={1} adjustsFontSizeToFit style={styles.text}>
						{item?.bid?.[1] || '-'}
					</Text>
				</View>
				<View style={styles.column}>
					<Text numberOfLines={1} adjustsFontSizeToFit style={styles.text}>
						{item?.bid?.[2] || '-'}
					</Text>
				</View>
			</View>
		</View>
	);
};

export default Row;
