import React, { useEffect, useState } from 'react';
import { Alert, Text, View } from 'react-native';

//Third party libraries
import auth from '@react-native-firebase/auth';

//Styles
import styles from './styles';
import { getUserData, setUserData } from '@libs/authHelper';
import { Button, Dialog, TextInput } from 'react-native-paper';
import Icon from '@components/Icon';
import { storage } from '@libs/localStoragePersist';
import { useAppContext } from '@store/AppContext';

const ProfilePage = ({ navigation }) => {
	const [data, setData] = useState({});
	const [editVisible, setEditVisible] = useState(false);
	const [name, setName] = useState('');
	const [surName, setSurName] = useState('');

	const { resetStates } = useAppContext();

	const [isLoggedIn, setIsLoggedIn] = useState(true);

	const hideEditModal = (reset = true) => {
		setEditVisible(false);
		if (reset) setData({ ...data });
	};
	const showEditModal = () => setEditVisible(true);

	useEffect(() => {
		getInfo();
	}, []);

	const getInfo = () => {
		getUserData(auth().currentUser.uid)
			.then(res => {
				setData(res?._data);
			})
			.finally(() => {
				hideEditModal(false);
			});
	};

	const save = () => {
		const params = { name, surName };
		setUserData(auth().currentUser.uid, params)
			.then(res => {
				setData(params);
			})
			.catch(err => {
				Alert.alert('Hata', 'Profil Bilgileri Güncellenemedi');
			})
			.finally(() => {
				hideEditModal(false);
			});
	};

	useEffect(() => {
		setName(data?.name);
		setSurName(data?.surName);
	}, [data]);

	const logOut = () => {
		setIsLoggedIn(false);

		setTimeout(() => {
			auth()
				.signOut()
				.then(() => {
					storage.clearAll();
					resetStates();
					navigation.replace('AuthNavigator');
				})
				.catch(() => {
					setIsLoggedIn(true);
				});
		}, 1500);
	};

	return (
		<View style={[styles.container, !isLoggedIn && { alignItems: 'center', justifyContent: 'center' }]}>
			<Text style={styles.welcomeText}>
				{isLoggedIn ? 'Merhaba' : 'Hoşçakal'},{'\n'}
				<Text style={styles.nameText}>{data?.name}</Text>
				{data?.surName}
			</Text>
			{isLoggedIn && (
				<>
					<Button
						mode="outlined"
						onPress={showEditModal}
						color="white"
						icon={() => <Icon name="edit : feather" size={15} color="white" />}
						style={styles.editButton}>
						Bilgiler Düzenle
					</Button>
					<Button
						onPress={logOut}
						mode="outlined"
						color="white"
						icon={() => <Icon name="power-off : font" size={15} color="white" />}
						style={styles.logOutButton}>
						Oturumu Kapat
					</Button>
					<Dialog visible={editVisible} onDismiss={hideEditModal}>
						<Dialog.Title>Bilgileri Düzenle</Dialog.Title>
						<Dialog.Content>
							<TextInput label={'Ad'} value={name} onChangeText={setName} />
							<TextInput label={'Soyad'} value={surName} onChangeText={setSurName} />
							<Button mode="outlined" onPress={save}>
								Kaydet
							</Button>
						</Dialog.Content>
					</Dialog>
				</>
			)}
		</View>
	);
};

export default ProfilePage;
