import { StyleSheet } from 'react-native';

import { colors } from '@styles/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: colors.background,
		padding: 40,
	},
	welcomeText: {
		color: 'white',
		fontSize: 25,
		marginVertical: 40,
		fontWeight: '100',
	},
	nameText: {
		fontSize: 35,
		fontWeight: 'bold',
	},
	editButton: {
		backgroundColor: colors.primary,
	},
	logOutButton: {
		backgroundColor: colors.red,
		marginTop: 20,
	},
});

export default styles;
