import React, { useEffect } from 'react';
import { Text, View } from 'react-native';

//Styles
import styles from './styles';

const LoadingPage = ({ navigation }) => {
	useEffect(() => {
		navigation.replace('AuthNavigator');
	}, []);

	return (
		<View>
			<Text>Loding</Text>
		</View>
	);
};

export default LoadingPage;
