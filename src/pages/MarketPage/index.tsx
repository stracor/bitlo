import React, { useEffect, useState } from 'react';
import { FlatList, View } from 'react-native';

//Components
import ListItem from './components/ListItem';
import MarketDetailModal from '@components/MarketDetailModal';

//Types
import { IMarket } from '@models/types';

//Store
import { useAppContext } from '@store/AppContext';

//Styles and services
import styles from './styles';
import coinService from '@services/coin-service';

const MarketPage = ({ navigation }) => {
	// Coin listesine heryerden erişebilmek adına bu şekilde kullandım yoksa global olarak tutmazdım
	const { markets, setMarkets } = useAppContext();

	const [selectedItem, setSelectedItem] = useState<IMarket>(null);

	const unSelectItem = () => setSelectedItem(null);

	const selectItem = item => () => setSelectedItem(item);

	const renderItem = props => <ListItem onPress={selectItem(props?.item)} {...props} />;

	const keyExtractor = item => item?.marketCode + 'Market';

	useEffect(() => {
		getMarket();
	}, []);

	const getMarket = () => {
		coinService.getMarkets().then(res => {
			setMarkets(res?.data);
		});
	};

	const navigate = () => {
		navigation.navigate('CoinDetail', { item: selectedItem });
		unSelectItem();
	};

	return (
		<View style={styles.container}>
			<FlatList keyExtractor={keyExtractor} contentContainerStyle={{ paddingBottom: 80 }} data={markets} renderItem={renderItem} />
			<MarketDetailModal item={selectedItem} navigate={navigate} unSelectItem={unSelectItem} />
		</View>
	);
};

export default MarketPage;
