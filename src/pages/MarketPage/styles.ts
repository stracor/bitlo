import { StyleSheet } from 'react-native';

import { colors } from '@styles/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: colors.background,
	},
});

export default styles;
