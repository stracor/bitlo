import React from 'react';
import { Pressable, Text, View } from 'react-native';

//Components
import CoinIcon from '@components/CoinIcon';
import { colors } from '@styles/theme';

//Styles
import styles from './style';

const ListItem = ({ item, onPress }) => {
	const stat = item?.change24hPercent < 0;

	return (
		<Pressable
			onPress={onPress}
			style={[styles.container, { borderColor: stat ? colors.red : colors.green, backgroundColor: stat ? colors.red + '40' : colors.green + '40' }]}>
			<View style={[styles.verticalContainer, { alignItems: 'center', justifyContent: 'center' }]}>
				<Text numberOfLines={1} adjustsFontSizeToFit style={styles.coinName}>
					{item?.marketCode}
				</Text>
				<View style={[styles.iconContainer, { backgroundColor: stat ? colors.red + '30' : colors.green + '30' }]}>
					<CoinIcon name={item?.marketCode} />
				</View>
			</View>
			<View style={styles.verticalContainer}>
				<View>
					<Text style={styles.subTitle}>24s E.Y.</Text>
					<Text style={styles.valueText} numberOfLines={1} adjustsFontSizeToFit>
						{item?.highestQuote24h}
					</Text>
				</View>
				<View>
					<Text style={styles.subTitle}>24s E.D.</Text>
					<Text style={styles.valueText} numberOfLines={1} adjustsFontSizeToFit>
						{item?.lowestQuote24h}
					</Text>
				</View>
			</View>

			<View style={styles.verticalContainer}>
				<View>
					<Text style={styles.subTitle}>24s Değ.</Text>
					<Text style={styles.valueText} numberOfLines={1} adjustsFontSizeToFit>
						%{item?.change24h}
					</Text>
				</View>
				<View>
					<Text style={styles.subTitle}>24s Ort.</Text>
					<Text style={styles.valueText} numberOfLines={1} adjustsFontSizeToFit>
						{item?.weightedAverage24h}
					</Text>
				</View>
			</View>

			<View style={styles.verticalContainer}>
				<View>
					<Text style={styles.subTitle}>24s E.Y.</Text>
					<Text style={styles.valueText} numberOfLines={1} adjustsFontSizeToFit>
						{item?.highestQuote24h}
					</Text>
				</View>
				<View>
					<Text style={styles.subTitle}>24s E.D.</Text>
					<Text style={styles.valueText} numberOfLines={1} adjustsFontSizeToFit>
						{item?.lowestQuote24h}
					</Text>
				</View>
			</View>
			<View style={[styles.verticalContainer, { justifyContent: 'center' }]}>
				<Text style={styles.subTitle}>Değer</Text>
				<Text style={[styles.valueText, { fontSize: 14 }]} numberOfLines={1} adjustsFontSizeToFit>
					{item?.lowestQuote24h}
				</Text>
			</View>
		</Pressable>
	);
};

export default ListItem;
