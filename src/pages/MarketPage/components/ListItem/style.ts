import { StyleSheet } from 'react-native';

import { colors } from '@styles/theme';
const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		padding: 10,
		borderLeftWidth: 3,
		height: 90,
		borderColor: 'white',
		margin: 10,
		marginBottom: 0,
		justifyContent: 'space-between',
		borderRadius: 0,
	},
	iconContainer: {
		width: 45,
		height: 45,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.secondary + '50',
		borderRadius: 10,
	},
	coinName: {
		color: 'white',
		fontSize: 10,
		marginBottom: 10,
		fontWeight: 'bold',
	},
	verticalContainer: {
		justifyContent: 'space-between',
		flex: 1,
	},
	subTitle: {
		fontSize: 12,
		color: colors.textGray,
	},
	valueText: {
		fontSize: 12,
		fontWeight: 'bold',
		color: 'white',
	},
});

export default styles;
