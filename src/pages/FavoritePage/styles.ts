import { StyleSheet } from 'react-native';

import { colors } from '@styles/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: colors.background,
	},
	description: {
		color: 'white',
		margin: 60,
		fontSize: 20,
		fontWeight: '100',
		alignSelf: 'center',
		textAlign: 'center',
	},
});

export default styles;
