import React, { useEffect, useState } from 'react';
import { FlatList, Text, View } from 'react-native';

//Components
import ListItem from '@pages/MarketPage/components/ListItem';
import MarketDetailModal from '@components/MarketDetailModal';

//Store
import { useAppContext } from '@store/AppContext';

//Types
import { IMarket } from '@models/types';

//Styles and services
import styles from './styles';

const FavoritePage = ({ navigation }) => {
	const { favorites } = useAppContext();
	const data = Object.values(favorites);

	const [selectedItem, setSelectedItem] = useState<IMarket>(null);

	const unSelectItem = () => setSelectedItem(null);

	const selectItem = item => () => setSelectedItem(item);

	const renderItem = props => <ListItem onPress={selectItem(props?.item)} {...props} />;

	const keyExtractor = item => item?.marketCode + 'Market';

	const navigate = () => {
		navigation.navigate('CoinDetail', { item: selectedItem });
		unSelectItem();
	};

	return (
		<View style={styles.container}>
			<FlatList
				keyExtractor={keyExtractor}
				contentContainerStyle={{ paddingBottom: 80 }}
				data={data}
				ListEmptyComponent={<Text style={styles.description}>Favorilerinize coin ekleyiniz.</Text>}
				renderItem={renderItem}
			/>
			<MarketDetailModal item={selectedItem} navigate={navigate} unSelectItem={unSelectItem} />
		</View>
	);
};

export default FavoritePage;
