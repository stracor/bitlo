import React from 'react';
import { View } from 'react-native';

//Styles
import styles from './style';

const BlankPage = () => {
	return <View style={styles.container}></View>;
};

export default BlankPage;
