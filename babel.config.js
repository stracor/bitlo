module.exports = {
	presets: ['module:metro-react-native-babel-preset'],
	plugins: [
		'react-native-reanimated/plugin',
		[
			'module-resolver',
			{
				root: ['./src'],
				extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
				alias: {
					'@assets': './src/assets',
					'@configs': './src/configs',
					'@pages': './src/pages',
					'@components': './src/shared/components',
					'@store': './src/shared/store',
					'@libs': './src/shared/libs',
					'@routers': './src/shared/routers',
					'@services': './src/shared/services',
					'@styles': './src/shared/styles',
					'@models': './src/shared/models',
				},
			},
		],
	],
};
