/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as appName } from './app.json';
import 'react-native-gesture-handler';
import 'intl';
import 'intl/locale-data/jsonp/tr-TR';
import '@libs/prototypes';
AppRegistry.registerComponent(appName, () => App);
